import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './registerServiceWorker'
import VeeValidate from 'vee-validate'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faShoppingCart, faChevronDown, faStar, faStarHalf, faEnvelope, faCompressArrowsAlt, faHeart } from '@fortawesome/free-solid-svg-icons'
import { faFacebookF, faTwitter, faInstagram, faPinterest } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faFacebookF, faTwitter, faInstagram, faPinterest, faShoppingCart, faChevronDown, faStar, faStarHalf, faEnvelope, faCompressArrowsAlt, faHeart)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(VeeValidate);

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
