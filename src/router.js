import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import ProductOverview from './views/ProductOverview.vue'
import ProductSingle from './views/ProductSingle.vue'
import Brand from './views/Brand.vue'
import SignIn from './views/SignIn.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/products',
      name: 'product-overview',
      component: ProductOverview
    },
    {
      path: '/products/:id',
      name: 'product-single',
      component: ProductSingle
    },
    {
      path: '/brand',
      name: 'brand',
      component: Brand
    },
    {
      path: '/sign-in',
      name: 'sign-in',
      component: SignIn
    }
  ]
})
